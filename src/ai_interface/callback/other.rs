use std::ffi::CString;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::{
    callback::{
        command::{
            command_data::{
                other::{PauseCommandData, SetLastPosMessageCommandData},
                CommandData, EmptyCommandData,
            },
            command_topic::CommandTopic,
        },
        engine::handle_command,
    },
    AIInterface,
};

impl AIInterface {
    pub fn set_last_message_position(&self, position: [f32; 3]) -> Result<(), &'static str> {
        let mut command_data = SetLastPosMessageCommandData { position };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::SetLastPOSMessage.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn pause<S>(&self, enable: bool, reason: S) -> Result<(), &'static str>
    where
        S: Into<Vec<u8>>,
    {
        let mut command_data = PauseCommandData {
            enable,
            reason: CString::new(reason).unwrap(),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::Pause.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn null(&self) -> Result<(), &'static str> {
        let mut command_data = EmptyCommandData {};

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::Null.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unused0(&self) -> Result<(), &'static str> {
        let mut command_data = EmptyCommandData {};

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::Unused0.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unused1(&self) -> Result<(), &'static str> {
        let mut command_data = EmptyCommandData {};

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::Unused1.into(),
            &mut command_data.c_data(),
        )
    }
}
