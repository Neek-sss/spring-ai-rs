use std::ffi::CString;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::{
    callback::{
        command::{
            command_data::{
                send::{
                    SendResourcesCommandData, SendStartPosCommandData, SendTextMessageCommandData,
                    SendUnitsCommandData,
                },
                CommandData,
            },
            command_topic::CommandTopic,
        },
        engine::handle_command,
        resource::Resource,
        teams::Team,
        unit::Unit,
    },
    AIInterface,
};

pub struct Send {
    pub ai_id: i32,
}

impl AIInterface {
    pub fn send(&self) -> Send {
        Send { ai_id: self.ai_id }
    }
}

impl Send {
    pub fn resources(
        &self,
        resource: &Resource,
        amount: f32,
        receiving_team: &Team,
    ) -> Result<(), &'static str> {
        let mut command_data = SendResourcesCommandData {
            resource_id: resource.resource_id,
            amount: 0.0,
            receiving_team_id: receiving_team.team_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::SendResources.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn start_position(&self, ready: bool, position: [f32; 3]) -> Result<(), &'static str> {
        let mut command_data = SendStartPosCommandData { ready, position };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::SendStartPOS.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn text_message<S>(&self, message: S, zone: i32) -> Result<(), &'static str>
    where
        S: Into<Vec<u8>>,
    {
        let mut command_data = SendTextMessageCommandData {
            text: CString::new(message).unwrap(),
            zone,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::SendTextMessage.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn units(&self, units: &[Unit]) -> Result<(), &'static str> {
        let mut command_data = SendUnitsCommandData {
            unit_ids: units.iter().map(|u| u.unit_id).collect(),
            receiving_team_id: 0,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::SendUnits.into(),
            &mut command_data.c_data(),
        )
    }
}
