use serde::{Deserialize, Serialize};
use spring_ai_sys::UNIT_COMMAND_BUILD_NO_FACING;

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum Facing {
    North,
    South,
    East,
    West,
    None,
}

impl Into<i32> for Facing {
    fn into(self) -> i32 {
        match self {
            Facing::North => 2,
            Facing::South => 0,
            Facing::East => 1,
            Facing::West => 3,
            Facing::None => UNIT_COMMAND_BUILD_NO_FACING,
        }
    }
}

impl Into<Facing> for i32 {
    fn into(self) -> Facing {
        match self {
            2 => Facing::North,
            0 => Facing::South,
            1 => Facing::East,
            3 => Facing::West,
            UNIT_COMMAND_BUILD_NO_FACING => Facing::None,
            _ => unimplemented!(),
        }
    }
}
