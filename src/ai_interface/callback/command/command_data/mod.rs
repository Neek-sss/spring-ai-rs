pub mod cheats;
pub mod debug_drawer;
pub mod drawer;
pub mod group;
pub mod lua;
pub mod other;
pub mod path;
pub mod send;
pub mod trace;
pub mod unit;

use libc::c_void;

// Traits
pub trait CommandData {
    type CDataType: CData;
    fn c_data(&mut self) -> Self::CDataType;
}

pub trait CData {
    fn c_void(&mut self) -> *mut c_void {
        self as *mut _ as *mut c_void
    }
}

// Empty data
pub struct EmptyCommandData {}

impl CommandData for EmptyCommandData {
    type CDataType = EmptyCommandCData;

    fn c_data(&mut self) -> Self::CDataType {
        EmptyCommandCData {}
    }
}

pub struct EmptyCommandCData {}

impl CData for EmptyCommandCData {}
