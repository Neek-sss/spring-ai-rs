use std::ffi::CString;

use spring_ai_sys::{
    SSendResourcesCommand, SSendStartPosCommand, SSendTextMessageCommand, SSendUnitsCommand,
};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Send resources data
pub struct SendResourcesCommandData {
    pub resource_id: i32,
    pub amount: f32,
    pub receiving_team_id: i32,
}

impl CommandData for SendResourcesCommandData {
    type CDataType = SSendResourcesCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSendResourcesCommand {
            resourceId: self.resource_id,
            amount: self.amount,
            receivingTeamId: self.receiving_team_id,
            ret_isExecuted: false,
        }
    }
}

impl CData for SSendResourcesCommand {}

// Send start position data
pub struct SendStartPosCommandData {
    pub ready: bool,
    pub position: [f32; 3],
}

impl CommandData for SendStartPosCommandData {
    type CDataType = SSendStartPosCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSendStartPosCommand {
            ready: self.ready,
            pos_posF3: self.position.as_mut_ptr(),
        }
    }
}

impl CData for SSendStartPosCommand {}

// Send text message data
pub struct SendTextMessageCommandData {
    pub text: CString,
    pub zone: i32,
}

impl CommandData for SendTextMessageCommandData {
    type CDataType = SSendTextMessageCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSendTextMessageCommand {
            text: self.text.as_ptr(),
            zone: self.zone,
        }
    }
}

impl CData for SSendTextMessageCommand {}

// Send units data
pub struct SendUnitsCommandData {
    pub unit_ids: Vec<i32>,
    pub receiving_team_id: i32,
}

impl CommandData for SendUnitsCommandData {
    type CDataType = SSendUnitsCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSendUnitsCommand {
            unitIds: self.unit_ids.as_mut_ptr(),
            unitIds_size: self.unit_ids.len() as i32,
            receivingTeamId: self.receiving_team_id,
            ret_sentUnits: 0,
        }
    }
}

impl CData for SSendUnitsCommand {}
