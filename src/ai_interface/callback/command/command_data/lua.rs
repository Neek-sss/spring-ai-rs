use std::ffi::CString;

use spring_ai_sys::{SCallLuaRulesCommand, SCallLuaUICommand};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Call Lua Rules data
pub struct CallLuaRulesCommandData {
    pub in_data: CString,
    pub in_size: i32,
    pub out_data: CString,
}

impl CommandData for CallLuaRulesCommandData {
    type CDataType = SCallLuaRulesCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCallLuaRulesCommand {
            inData: self.in_data.as_ptr(),
            inSize: self.in_size,
            ret_outData: self.out_data.clone().into_raw(),
        }
    }
}

impl CData for SCallLuaRulesCommand {}

// Call Lua UI data
pub struct CallLuaUICommandData {
    pub in_data: CString,
    pub in_size: i32,
    pub out_data: CString,
}

impl CommandData for CallLuaUICommandData {
    type CDataType = SCallLuaUICommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCallLuaUICommand {
            inData: self.in_data.as_ptr(),
            inSize: self.in_size,
            ret_outData: self.out_data.clone().into_raw(),
        }
    }
}

impl CData for SCallLuaUICommand {}
