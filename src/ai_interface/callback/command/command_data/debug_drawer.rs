use std::ffi::CString;

use spring_ai_sys::{
    SAddOverlayTextureDrawerDebugCommand, SAddPointLineGraphDrawerDebugCommand,
    SDeleteOverlayTextureDrawerDebugCommand, SDeletePointsLineGraphDrawerDebugCommand,
    SSetColorLineGraphDrawerDebugCommand, SSetLabelLineGraphDrawerDebugCommand,
    SSetLabelOverlayTextureDrawerDebugCommand, SSetPositionGraphDrawerDebugCommand,
    SSetPositionOverlayTextureDrawerDebugCommand, SSetSizeGraphDrawerDebugCommand,
    SSetSizeOverlayTextureDrawerDebugCommand, SUpdateOverlayTextureDrawerDebugCommand,
};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Graph Line Add Point data
pub struct AddPointLineGraphDrawerDebugCommandData {
    pub line_id: i32,
    pub x: f32,
    pub y: f32,
}

impl CommandData for AddPointLineGraphDrawerDebugCommandData {
    type CDataType = SAddPointLineGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAddPointLineGraphDrawerDebugCommand {
            lineId: self.line_id,
            x: self.x,
            y: self.y,
        }
    }
}

impl CData for SAddPointLineGraphDrawerDebugCommand {}

// Graph Line Delete Point data
pub struct DeletePointsLineGraphDrawerDebugCommandData {
    pub line_id: i32,
    pub num_points: i32,
}

impl CommandData for DeletePointsLineGraphDrawerDebugCommandData {
    type CDataType = SDeletePointsLineGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDeletePointsLineGraphDrawerDebugCommand {
            lineId: self.line_id,
            numPoints: self.num_points,
        }
    }
}

impl CData for SDeletePointsLineGraphDrawerDebugCommand {}

// Graph Line Set Color data
pub struct SetColorLineGraphDrawerDebugCommandData {
    pub line_id: i32,
    pub color: [i16; 3],
}

impl CommandData for SetColorLineGraphDrawerDebugCommandData {
    type CDataType = SSetColorLineGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetColorLineGraphDrawerDebugCommand {
            lineId: self.line_id,
            color_colorS3: self.color.as_mut_ptr(),
        }
    }
}

impl CData for SSetColorLineGraphDrawerDebugCommand {}

// Graph Line Set Label data
pub struct SetLabelLineGraphDrawerDebugCommandData {
    pub line_id: i32,
    pub label: CString,
}

impl CommandData for SetLabelLineGraphDrawerDebugCommandData {
    type CDataType = SSetLabelLineGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetLabelLineGraphDrawerDebugCommand {
            lineId: self.line_id,
            label: self.label.as_ptr(),
        }
    }
}

impl CData for SSetLabelLineGraphDrawerDebugCommand {}

// Graph Set Position data
pub struct SetPositionGraphDrawerDebugCommandData {
    pub x: f32,
    pub y: f32,
}

impl CommandData for SetPositionGraphDrawerDebugCommandData {
    type CDataType = SSetPositionGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetPositionGraphDrawerDebugCommand {
            x: self.x,
            y: self.y,
        }
    }
}

impl CData for SSetPositionGraphDrawerDebugCommand {}

// Graph Set Size data
pub struct SetSizeGraphDrawerDebugCommandData {
    pub w: f32,
    pub h: f32,
}

impl CommandData for SetSizeGraphDrawerDebugCommandData {
    type CDataType = SSetSizeGraphDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetSizeGraphDrawerDebugCommand {
            w: self.w,
            h: self.h,
        }
    }
}

impl CData for SSetSizeGraphDrawerDebugCommand {}

// Add Overlay Texture data
pub struct AddOverlayTextureDrawerDebugCommandData {
    pub texture_data: Vec<f32>,
    pub w: i32,
    pub h: i32,
}

impl CommandData for AddOverlayTextureDrawerDebugCommandData {
    type CDataType = SAddOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAddOverlayTextureDrawerDebugCommand {
            texData: self.texture_data.as_ptr(),
            w: self.w,
            h: self.h,
            ret_overlayTextureId: 0,
        }
    }
}

impl CData for SAddOverlayTextureDrawerDebugCommand {}

// Delete Overlay Texture data
pub struct DeleteOverlayTextureDrawerDebugCommandData {
    pub overlay_texture_id: i32,
}

impl CommandData for DeleteOverlayTextureDrawerDebugCommandData {
    type CDataType = SDeleteOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDeleteOverlayTextureDrawerDebugCommand {
            overlayTextureId: self.overlay_texture_id,
        }
    }
}

impl CData for SDeleteOverlayTextureDrawerDebugCommand {}

// Set Overlay Texture Label data
pub struct SetLabelOverlayTextureDrawerDebugCommandData {
    pub overlay_texture_id: i32,
    pub label: CString,
}

impl CommandData for SetLabelOverlayTextureDrawerDebugCommandData {
    type CDataType = SSetLabelOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetLabelOverlayTextureDrawerDebugCommand {
            overlayTextureId: self.overlay_texture_id,
            label: self.label.as_ptr(),
        }
    }
}

impl CData for SSetLabelOverlayTextureDrawerDebugCommand {}

// Set Overlay Texture Position data
pub struct SetPositionOverlayTextureDrawerDebugCommandData {
    pub overlay_texture_id: i32,
    pub x: f32,
    pub y: f32,
}

impl CommandData for SetPositionOverlayTextureDrawerDebugCommandData {
    type CDataType = SSetPositionOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetPositionOverlayTextureDrawerDebugCommand {
            overlayTextureId: self.overlay_texture_id,
            x: self.x,
            y: self.y,
        }
    }
}

impl CData for SSetPositionOverlayTextureDrawerDebugCommand {}

// Set Overlay Texture Size data
pub struct SetSizeOverlayTextureDrawerDebugCommandData {
    pub overlay_texture_id: i32,
    pub w: f32,
    pub h: f32,
}

impl CommandData for SetSizeOverlayTextureDrawerDebugCommandData {
    type CDataType = SSetSizeOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetSizeOverlayTextureDrawerDebugCommand {
            overlayTextureId: self.overlay_texture_id,
            w: self.w,
            h: self.h,
        }
    }
}

impl CData for SSetSizeOverlayTextureDrawerDebugCommand {}

// Update Overlay Texture data
pub struct UpdateOverlayTextureDrawerDebugCommandData {
    pub overlay_texture_id: i32,
    pub tex_data: Vec<f32>,
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

impl CommandData for UpdateOverlayTextureDrawerDebugCommandData {
    type CDataType = SUpdateOverlayTextureDrawerDebugCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SUpdateOverlayTextureDrawerDebugCommand {
            overlayTextureId: self.overlay_texture_id,
            texData: self.tex_data.as_ptr(),
            x: self.x,
            y: self.y,
            w: self.w,
            h: self.h,
        }
    }
}

impl CData for SUpdateOverlayTextureDrawerDebugCommand {}
