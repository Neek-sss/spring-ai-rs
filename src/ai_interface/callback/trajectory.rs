use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum Trajectory {
    Low,
    High,
}

impl Into<i32> for Trajectory {
    fn into(self) -> i32 {
        match self {
            Trajectory::Low => 0,
            Trajectory::High => 1,
        }
    }
}

impl Into<Trajectory> for i32 {
    fn into(self) -> Trajectory {
        match self {
            0 => Trajectory::Low,
            1 => Trajectory::High,
            _ => unimplemented!(),
        }
    }
}
