use crate::{ai_interface::AIInterface, get_callback};
use std::error::Error;

#[derive(Debug, Copy, Clone)]
pub struct GUI {
    ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct GUIAll {
    view_range: f32,
    screen_xy: (f32, f32),
    screen_direction: [f32; 3],
    screen_position: [f32; 3],
}

impl AIInterface {
    pub fn gui(&self) -> GUI {
        GUI { ai_id: self.ai_id }
    }
}

impl GUI {
    pub fn view_range(&self) -> Result<f32, Box<dyn Error>> {
        let view_range_func = get_callback!(self.ai_id, Gui_getViewRange)?;
        Ok(unsafe { view_range_func(self.ai_id) })
    }

    pub fn screen_xy(&self) -> Result<(f32, f32), Box<dyn Error>> {
        let screen_x_func = get_callback!(self.ai_id, Gui_getScreenX)?;
        let screen_y_func = get_callback!(self.ai_id, Gui_getScreenY)?;

        Ok((unsafe { screen_x_func(self.ai_id) }, unsafe {
            screen_y_func(self.ai_id)
        }))
    }

    pub fn screen_direction(&self) -> Result<[f32; 3], Box<dyn Error>> {
        let direction_func = get_callback!(self.ai_id, Gui_Camera_getDirection)?;

        let mut direction = [0.0_f32; 3];
        unsafe { direction_func(self.ai_id, direction.as_mut_ptr()) }

        Ok(direction)
    }

    pub fn screen_position(&self) -> Result<[f32; 3], Box<dyn Error>> {
        let position_func = get_callback!(self.ai_id, Gui_Camera_getPosition)?;

        let mut position = [0.0_f32; 3];
        unsafe { position_func(self.ai_id, position.as_mut_ptr()) }

        Ok(position)
    }

    pub fn all(&self) -> Result<GUIAll, Box<dyn Error>> {
        Ok(GUIAll {
            view_range: self.view_range()?,
            screen_xy: self.screen_xy()?,
            screen_direction: self.screen_direction()?,
            screen_position: self.screen_position()?,
        })
    }
}
