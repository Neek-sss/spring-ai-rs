use std::{error::Error, f32::NAN};

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GroupOrderPreview {
    pub ai_id: i32,
    pub group_id: i32,
    pub group_order_preview_id: i32,
}

#[derive(Debug, Clone)]
pub struct GroupOrderPreviewAll {
    options: i16,
    tag: i32,
    timeout: i32,
    parameters: Vec<f32>,
}

const MAX_PARAMETERS: usize = 32;

impl GroupOrderPreview {
    pub fn options(&self) -> Result<i16, Box<dyn Error>> {
        let get_options_func = get_callback!(self.ai_id, Group_OrderPreview_getOptions)?;

        Ok(unsafe { get_options_func(self.ai_id, self.group_id) })
    }

    pub fn tag(&self) -> Result<i32, Box<dyn Error>> {
        let get_tag_func = get_callback!(self.ai_id, Group_OrderPreview_getTag)?;

        Ok(unsafe { get_tag_func(self.ai_id, self.group_id) })
    }

    pub fn timeout(&self) -> Result<i32, Box<dyn Error>> {
        let get_timeout_func = get_callback!(self.ai_id, Group_OrderPreview_getTimeOut)?;

        Ok(unsafe { get_timeout_func(self.ai_id, self.group_id) })
    }

    pub fn parameters(&self) -> Result<Vec<f32>, Box<dyn Error>> {
        let get_order_preview_parameters_func =
            get_callback!(self.ai_id, Group_OrderPreview_getParams)?;

        let mut f = [NAN; MAX_PARAMETERS];
        unsafe {
            get_order_preview_parameters_func(
                self.ai_id,
                self.group_id,
                f.as_mut_ptr(),
                MAX_PARAMETERS as i32,
            )
        };

        Ok(f.iter().cloned().filter(|&f_i| f_i != NAN).collect())
    }

    pub fn all(&self) -> Result<GroupOrderPreviewAll, Box<dyn Error>> {
        Ok(GroupOrderPreviewAll {
            options: self.options()?,
            tag: self.tag()?,
            timeout: self.timeout()?,
            parameters: self.parameters()?,
        })
    }
}
