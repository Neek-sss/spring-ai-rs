use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitVision {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitVisionAll {
    pub los_radius: f32,
    pub air_los_radius: f32,
    pub los_height: f32,
    pub radar_radius: i32,
    pub sonar_radius: i32,
}

impl UnitVision {
    pub fn los_radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_los_radius_func = get_callback!(self.ai_id, UnitDef_getLosRadius)?;
        Ok(unsafe { get_los_radius_func(self.ai_id, self.def_id) })
    }

    pub fn air_los_radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_air_los_radius_func = get_callback!(self.ai_id, UnitDef_getAirLosRadius)?;
        Ok(unsafe { get_air_los_radius_func(self.ai_id, self.def_id) })
    }

    pub fn los_height(&self) -> Result<f32, Box<dyn Error>> {
        let get_los_height_func = get_callback!(self.ai_id, UnitDef_getLosHeight)?;
        Ok(unsafe { get_los_height_func(self.ai_id, self.def_id) })
    }

    pub fn radar_radius(&self) -> Result<i32, Box<dyn Error>> {
        let get_radar_radius_func = get_callback!(self.ai_id, UnitDef_getRadarRadius)?;
        Ok(unsafe { get_radar_radius_func(self.ai_id, self.def_id) })
    }

    pub fn sonar_radius(&self) -> Result<i32, Box<dyn Error>> {
        let get_sonar_radius_func = get_callback!(self.ai_id, UnitDef_getSonarRadius)?;
        Ok(unsafe { get_sonar_radius_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitVisionAll, Box<dyn Error>> {
        Ok(UnitVisionAll {
            los_radius: self.los_radius()?,
            air_los_radius: self.air_los_radius()?,
            los_height: self.los_height()?,
            radar_radius: self.radar_radius()?,
            sonar_radius: self.sonar_radius()?,
        })
    }
}
