use std::{collections::HashMap, error::Error, ffi::CStr, hash::Hash, str::FromStr};

use serde::{Deserialize, Serialize};

use crate::{
    ai_interface::{
        callback::{
            resource::Resource,
            unit_def::{
                aircraft::{UnitAircraft, UnitAircraftAll},
                auto_heal::{UnitAutoHeal, UnitAutoHealAll},
                building::{UnitBuilding, UnitBuildingAll},
                cloak::{UnitCloak, UnitCloakAll},
                combat::{UnitCombat, UnitCombatAll},
                construction::{UnitConstruction, UnitConstructionAll},
                death::{UnitDeath, UnitDeathAll},
                detection::{UnitDetection, UnitDetectionAll},
                flanking::{UnitFlanking, UnitFlankingAll},
                flare::{UnitFlare, UnitFlareAll},
                kind::base::UnitDefKind,
                miscellaneous::{UnitMiscellaneous, UnitMiscellaneousAll},
                model::{UnitModel, UnitModelAll},
                movement::{UnitMovement, UnitMovementAll},
                resource::{UnitResource, UnitResourceAll},
                transport::{UnitTransport, UnitTransportAll},
                vision::{UnitVision, UnitVisionAll},
                weapon_mount::UnitWeaponMountAll,
            },
        },
        AIInterface,
    },
    get_callback,
};

pub mod aircraft;
pub mod auto_heal;
pub mod building;
pub mod cloak;
pub mod combat;
pub mod construction;
pub mod death;
pub mod detection;
pub mod flanking;
pub mod flare;
pub mod kind;
pub mod miscellaneous;
pub mod model;
pub mod movement;
pub mod resource;
pub mod transport;
pub mod vision;
pub mod weapon_mount;

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct UnitDef {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitDefAll {
    pub name: String,
    pub display_name: String,
    pub kind: UnitDefKind,
    pub model: UnitModelAll,
    pub resource: HashMap<Resource, UnitResourceAll>,
    pub construction: UnitConstructionAll,
    pub auto_heal: UnitAutoHealAll,
    pub movement: UnitMovementAll,
    pub vision: UnitVisionAll,
    pub detection: UnitDetectionAll,
    pub building: UnitBuildingAll,
    pub flanking: UnitFlankingAll,
    pub combat: UnitCombatAll,
    pub death: UnitDeathAll,
    pub aircraft: UnitAircraftAll,
    pub transport: UnitTransportAll,
    pub cloak: UnitCloakAll,
    pub flare: UnitFlareAll,
    pub weapon_mounts: Vec<UnitWeaponMountAll>,
    pub miscellaneous: UnitMiscellaneousAll,
}

impl UnitDef {
    pub fn name(&self) -> Result<String, Box<dyn Error>> {
        let get_unit_def_name = get_callback!(self.ai_id, UnitDef_getName)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_unit_def_name(self.ai_id, self.def_id)) }.to_str()?,
        ))
    }

    pub fn display_name(&self) -> Result<String, Box<dyn Error>> {
        let get_unit_def_display_name = get_callback!(self.ai_id, UnitDef_getHumanName)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_unit_def_display_name(self.ai_id, self.def_id)) }
                .to_str()?,
        ))
    }

    pub fn kind(&self) -> Result<UnitDefKind, Box<dyn Error>> {
        Ok(UnitDefKind::from_str(&self.name()?)?)
    }

    pub fn model(&self) -> UnitModel {
        UnitModel {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn resource(&self, resource: Resource) -> UnitResource {
        UnitResource {
            ai_id: self.ai_id,
            def_id: self.def_id,
            resource,
        }
    }

    pub fn construction(&self) -> UnitConstruction {
        UnitConstruction {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn auto_heal(&self) -> UnitAutoHeal {
        UnitAutoHeal {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn movement(&self) -> UnitMovement {
        UnitMovement {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn vision(&self) -> UnitVision {
        UnitVision {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn detection(&self) -> UnitDetection {
        UnitDetection {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn building(&self) -> UnitBuilding {
        UnitBuilding {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn flanking(&self) -> UnitFlanking {
        UnitFlanking {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn combat(&self) -> UnitCombat {
        UnitCombat {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn death(&self) -> UnitDeath {
        UnitDeath {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn aircraft(&self) -> UnitAircraft {
        UnitAircraft {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn transport(&self) -> UnitTransport {
        UnitTransport {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn cloak(&self) -> UnitCloak {
        UnitCloak {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn flare(&self) -> UnitFlare {
        UnitFlare {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    pub fn miscellaneous(&self) -> UnitMiscellaneous {
        UnitMiscellaneous {
            ai_id: self.ai_id,
            def_id: self.def_id,
        }
    }

    // TODO:
    //    pub fn UnitDef_getYardMap(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getYardMap)?;
    //        Ok(())
    //    }

    //    pub fn UnitDef_isNeedGeo(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isNeedGeo)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isFeature(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isFeature)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isHideDamage(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isHideDamage)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isShowPlayerName(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isShowPlayerName)?;
    //        Ok(())
    //    }

    //    pub fn UnitDef_getHighTrajectoryType(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getHighTrajectoryType)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_getNoChaseCategory(&self) -> Result<(), Box<dyn Error>> {
    //        let get_no_chase_category_func = get_callback!(self.ai_id, UnitDef_getNoChaseCategory)?;
    //        Ok(())
    //    }
    //
    //
    //    pub fn UnitDef_isAbleToLoopbackAttack(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isAbleToLoopbackAttack)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isLevelGround(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_level_ground_func = get_callback!(self.ai_id, UnitDef_isLevelGround)?;
    //        Ok(())
    //    }
    //
    //    pub fn UnitDef_getDecoyDef(&self) -> Result<(), Box<dyn Error>> {
    //        let get_decoy_def_func = get_callback!(self.ai_id, UnitDef_getDecoyDef)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isDontLand(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isDontLand)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_getShieldDef(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getShieldDef)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_getStockpileDef(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getStockpileDef)?;
    //        Ok(())
    //    }
    //
    //    pub fn UnitDef_getCustomParams(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getCustomParams)?;
    //        Ok(())
    //    }
    //    pub fn UnitDef_isMoveDataAvailable(&self) -> Result<(), Box<dyn Error>> {
    //        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isMoveDataAvailable)?;
    //        Ok(())
    //    }

    pub fn all(&self) -> Result<UnitDefAll, Box<dyn Error>> {
        Ok(UnitDefAll {
            name: self.name()?,
            display_name: self.display_name()?,
            kind: self.kind()?,
            model: self.model().all()?,
            resource: AIInterface::new(self.ai_id)
                .resource_interface()
                .get_resources()?
                .into_iter()
                .filter_map(|resource| {
                    if let Ok(res) = self.resource(resource).all() {
                        Some((resource, res))
                    } else {
                        None
                    }
                })
                .collect(),
            construction: self.construction().all()?,
            auto_heal: self.auto_heal().all()?,
            movement: self.movement().all()?,
            vision: self.vision().all()?,
            detection: self.detection().all()?,
            building: self.building().all()?,
            flanking: self.flanking().all()?,
            combat: self.combat().all()?,
            death: self.death().all()?,
            aircraft: self.aircraft().all()?,
            transport: self.transport().all()?,
            cloak: self.cloak().all()?,
            flare: self.flare().all()?,
            weapon_mounts: self
                .weapon_mounts()?
                .into_iter()
                .filter_map(|weapon| weapon.all().ok())
                .collect(),
            miscellaneous: self.miscellaneous().all()?,
        })
    }
}
