use crate::ai_interface::callback::unit_def::{kind::base::UnitDefKind, UnitDef};

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum Core {
    // Commander
    Commander,
    WindTurbine,
    Solar,
    MetalStorage,
    EnergyStorage,
    MetalExtractor,
    EnergyConverter,
    BotLab,
    VehiclePlant,
    AirPlant,
    HovercraftPlatform,
    Camera,
    Radar,
    Teeth,
    LightLaserTower,
    LightAntiAir,
    FloatingLightAntiAir,
    TorpedoLauncher,
    UnderwaterMetalStorage,
    UnderwaterEnergyStorage,
    NavalEnergyConverter,
    Shipyard,
    TidalGenerator,
    FloatingTeeth,
    FloatingRadar,
    FloatingHovercraftPlatform,
    OffshoreTorpedoLauncher,
    // T1 Air Plant
    T1AirConstructor,
    T1AirScout,
    T1Fighter,
    T1Bomber,
    LightParalyzerDrone,
    T1AirTransport,
    // T1 Air Constructor
    AdvancedSolar,
    GeothermalPowerplant,
    ArmedMetalExtractor,
    AircraftRepairPad,
    UnderwaterAdvancedGeothermalPowerplant,
    AdvancedAircraftPlant,
    ConstructionTurret,
    FlamethrowerTurret,
    AntiSwarmLaserTower,
    AreaControlLaserTower,
    Artillery,
    AntiAirMissle,
    RadarJammer,
    AntiRadar,
    // T2 Air Plant
    T2AirConstructor,
    RadarSonarAircraft,
    StealthFighter,
    Gunship,
    FlyingFortress,
    StrategicBomber,
    TorpedoBomber,
    HeavyTransport,
    // T2 Air Constructor
    FusionReactor,
    AdvancedFusionReactor,
    AdvancedGeothermalPowerplant,
    GeothermalWeapon,
    Unknown0,
    AdvancedMetalExtractor,
    AdvancedArmoredMetalExtractor,
    WaterAircraftRepairPad,
    HardenedEnergyStorage,
    HardenedMetalStorage,
    AdvancedRadar,
    LongRangeJammer,
    FortificationWall,
    AdvancedEnergyConverter,
    IntrusionDetector,
    PlasmaDeflector,
    EnergyWeapon,
    RadarTargeting,
    PopUpBattery,
    PopUpPlasmaArtillery,
    AntiAirFlak,
    LongRangeAntiAir,
    SeaplanePlatform,
    TacticalMissileLauncher,
    NuclearSilo,
    AntiNukeLauncher,
    LongRangePlasmaCannon,
    RapidFireLongRangePlasmaCannon,
    ExperimentalGantry,
    // T1 Bot Lab
    T1BotConstructor,
    T1RessurectionBot,
    FastInfantryBot,
    RocketBot,
    LightPlasmaBot,
    AmphibiousAntiAirBot,
    // T1 Bot Constructor
    AdvancedBotLab,
    // T2 Bot Lab
    T2BotConstructor,
    FastAssaultBot,
    AmphibiousBot,
    HeavilyArmoredAssaultBot,
    ArmoredAssaultBot,
    SpiderBot,
    MortarBot,
    CrawlingBomb,
    HeavyRocketBot,
    AdvancedCrawlingBomb,
    DecoyCommander,
    RadarBot,
    SpyBot,
    StealthBot,
    CombatEngineer,
    JammerBot,
    HeavyAntiAir,
    // Stealth Bot
    Unknown1,
    // T2 Bot Constructor
    // T1 Vehicle Plant
    T1VehicleConstructor,
    AmphibiousConstructor,
    Minelayer,
    LightScoutVehicle,
    LightTank,
    LightAmphibiousTank,
    MediumAssaultTank,
    LightMobileArtillery,
    MissileTruck,
    AntiSwarmTank,
    // T1 Minelayer
    LightMine,
    MediumMine,
    HeavyMine,
    // T1 Vehicle Constructor
    AdvancedVehiclePlant,
    // T2 Vehicle Plant
    T2VehicleConstructor,
    RadarJammerVehicle,
    HeavyAssaultTank,
    StealthyRocketLauncher,
    HeavyMissileTank,
    MobileArtilleryTank,
    HeavyArtilleryTank,
    VeryHeavyAssaultTank,
    MediumAmphibiousTank,
    VeryHeavyAmphibiousTank,
    AntiAirFlakTank,
    AntiNukeTank,
    RadarVehicle,
    // Experimental Gantry
    MobileHeavyTurretExperimental,
    AllTerrainAssaultExperimental,
    AssaultBotExperimental,
    AmphibiousSiegeExperimental,
    HeavyRocketExperimental,
    HeavyLaserHovertankExperimental,
    // T1 Hovercraft Plant
    T1HovercraftConstructor,
    FastAttackHovertank,
    Hovertank,
    AssaultHovertank,
    RocketHovertank,
    AntiAirHovertank,
    // T1 Hovercraft Constructor
    NavalConstructionTurret,
    AmphibiousComplex,
    FloatingHeavyLaserTower,
    AdvancedShipyard,
    UnderwaterGeothermalPowerplant,
    // T1 Amphibious Constructor
    PopUpTorpedoLauncher,
    // T1 Shipyard
    T1ShipConstructor,
    RessurectionSub,
    LightGunBoat,
    MissileCorvette,
    AssaultFrigate,
    Destroyer,
    Sub,
    // T1 Ship Constructor
    // T2 Shipyard
    T2ShipConstructor,
    NavalEngineer,
    Cruiser,
    FastAssaultSubmarine,
    LongRangeBattleSubmarine,
    AntiAirShip,
    RadarJammerShip,
    AircraftCarrier,
    Battleship,
    CruiseMissileShip,
    Flagship,
    // Naval Engineer
    FloatingHeavyMine,
    // T2 Ship Constructor
    UnderwaterExperimentalGantry,
    UnderwaterFusionReactor,
    UnderwaterMetalConverter,
    UnderwaterEnergyConverter,
    AdvancedSonar,
    NavalAdvancedRadarTargeting,
    AdvancedTorpedoLauncher,
    NavalAntiAirGun,
    FloatingMultiweaponPlatform,
    // T1 Seaplane Plant
    SeaplaneConstructor,
    SeaplaneGunship,
    SeaplaneBomber,
    SeaplaneTorpedoGunship,
    SeaplaneFighter,
    SeaplaneRadarSonar,
    // T1 Seaplane Constructor
    Unknown,
}

impl PartialEq<Core> for UnitDef {
    fn eq(&self, other: &Core) -> bool {
        *other == self.kind().unwrap_or(UnitDefKind::Unknown)
    }
}

impl PartialEq<UnitDef> for Core {
    fn eq(&self, other: &UnitDef) -> bool {
        other.kind().unwrap_or(UnitDefKind::Unknown) == *self
    }
}

impl PartialEq<Core> for UnitDefKind {
    fn eq(&self, other: &Core) -> bool {
        other == self
    }
}

impl PartialEq<UnitDefKind> for Core {
    fn eq(&self, other: &UnitDefKind) -> bool {
        match other {
            UnitDefKind::CoreCommander => *self == Self::Commander,
            UnitDefKind::CoreWindTurbine => *self == Self::WindTurbine,
            UnitDefKind::CoreSolar => *self == Self::Solar,
            UnitDefKind::CoreMetalStorage => *self == Self::MetalStorage,
            UnitDefKind::CoreEnergyStorage => *self == Self::EnergyStorage,
            UnitDefKind::CoreMetalExtractor => *self == Self::MetalExtractor,
            UnitDefKind::CoreEnergyConverter => *self == Self::EnergyConverter,
            UnitDefKind::CoreBotLab => *self == Self::BotLab,
            UnitDefKind::CoreVehiclePlant => *self == Self::VehiclePlant,
            UnitDefKind::CoreAirPlant => *self == Self::AirPlant,
            UnitDefKind::CoreHovercraftPlatform => *self == Self::HovercraftPlatform,
            UnitDefKind::CoreCamera => *self == Self::Camera,
            UnitDefKind::CoreRadar => *self == Self::Radar,
            UnitDefKind::CoreTeeth => *self == Self::Teeth,
            UnitDefKind::CoreLightLaserTower => *self == Self::LightLaserTower,
            UnitDefKind::CoreLightAntiAir => *self == Self::LightAntiAir,
            UnitDefKind::CoreFloatingLightAntiAir => *self == Self::FloatingLightAntiAir,
            UnitDefKind::CoreTorpedoLauncher => *self == Self::TorpedoLauncher,
            UnitDefKind::CoreUnderwaterMetalStorage => *self == Self::UnderwaterMetalStorage,
            UnitDefKind::CoreUnderwaterEnergyStorage => *self == Self::UnderwaterEnergyStorage,
            UnitDefKind::CoreNavalEnergyConverter => *self == Self::NavalEnergyConverter,
            UnitDefKind::CoreShipyard => *self == Self::Shipyard,
            UnitDefKind::CoreTidalGenerator => *self == Self::TidalGenerator,
            UnitDefKind::CoreFloatingTeeth => *self == Self::FloatingTeeth,
            UnitDefKind::CoreFloatingRadar => *self == Self::FloatingRadar,
            UnitDefKind::CoreFloatingHovercraftPlatform => {
                *self == Self::FloatingHovercraftPlatform
            }
            UnitDefKind::CoreOffshoreTorpedoLauncher => *self == Self::OffshoreTorpedoLauncher,
            UnitDefKind::CoreT1AirConstructor => *self == Self::T1AirConstructor,
            UnitDefKind::CoreT1AirScout => *self == Self::T1AirScout,
            UnitDefKind::CoreT1Fighter => *self == Self::T1Fighter,
            UnitDefKind::CoreT1Bomber => *self == Self::T1Bomber,
            UnitDefKind::CoreLightParalyzerDrone => *self == Self::LightParalyzerDrone,
            UnitDefKind::CoreT1AirTransport => *self == Self::T1AirTransport,
            UnitDefKind::CoreAdvancedSolar => *self == Self::AdvancedSolar,
            UnitDefKind::CoreGeothermalPowerplant => *self == Self::GeothermalPowerplant,
            UnitDefKind::CoreArmedMetalExtractor => *self == Self::ArmedMetalExtractor,
            UnitDefKind::CoreAircraftRepairPad => *self == Self::AircraftRepairPad,
            UnitDefKind::CoreUnderwaterAdvancedGeothermalPowerplant => {
                *self == Self::UnderwaterAdvancedGeothermalPowerplant
            }
            UnitDefKind::CoreAdvancedAircraftPlant => *self == Self::AdvancedAircraftPlant,
            UnitDefKind::CoreConstructionTurret => *self == Self::ConstructionTurret,
            UnitDefKind::CoreFlamethrowerTurret => *self == Self::FlamethrowerTurret,
            UnitDefKind::CoreAntiSwarmLaserTower => *self == Self::AntiSwarmLaserTower,
            UnitDefKind::CoreAreaControlLaserTower => *self == Self::AreaControlLaserTower,
            UnitDefKind::CoreArtillery => *self == Self::Artillery,
            UnitDefKind::CoreAntiAirMissle => *self == Self::AntiAirMissle,
            UnitDefKind::CoreRadarJammer => *self == Self::RadarJammer,
            UnitDefKind::CoreAntiRadar => *self == Self::AntiRadar,
            UnitDefKind::CoreT2AirConstructor => *self == Self::T2AirConstructor,
            UnitDefKind::CoreRadarSonarAircraft => *self == Self::RadarSonarAircraft,
            UnitDefKind::CoreStealthFighter => *self == Self::StealthFighter,
            UnitDefKind::CoreGunship => *self == Self::Gunship,
            UnitDefKind::CoreFlyingFortress => *self == Self::FlyingFortress,
            UnitDefKind::CoreStrategicBomber => *self == Self::StrategicBomber,
            UnitDefKind::CoreTorpedoBomber => *self == Self::TorpedoBomber,
            UnitDefKind::CoreHeavyTransport => *self == Self::HeavyTransport,
            UnitDefKind::CoreFusionReactor => *self == Self::FusionReactor,
            UnitDefKind::CoreAdvancedFusionReactor => *self == Self::AdvancedFusionReactor,
            UnitDefKind::CoreAdvancedGeothermalPowerplant => {
                *self == Self::AdvancedGeothermalPowerplant
            }
            UnitDefKind::CoreGeothermalWeapon => *self == Self::GeothermalWeapon,
            UnitDefKind::CoreAdvancedMetalExtractor => *self == Self::AdvancedMetalExtractor,
            UnitDefKind::CoreAdvancedArmoredMetalExtractor => {
                *self == Self::AdvancedArmoredMetalExtractor
            }
            UnitDefKind::CoreWaterAircraftRepairPad => *self == Self::WaterAircraftRepairPad,
            UnitDefKind::CoreHardenedEnergyStorage => *self == Self::HardenedEnergyStorage,
            UnitDefKind::CoreHardenedMetalStorage => *self == Self::HardenedMetalStorage,
            UnitDefKind::CoreAdvancedRadar => *self == Self::AdvancedRadar,
            UnitDefKind::CoreLongRangeJammer => *self == Self::LongRangeJammer,
            UnitDefKind::CoreFortificationWall => *self == Self::FortificationWall,
            UnitDefKind::CoreAdvancedEnergyConverter => *self == Self::AdvancedEnergyConverter,
            UnitDefKind::CoreIntrusionDetector => *self == Self::IntrusionDetector,
            UnitDefKind::CorePlasmaDeflector => *self == Self::PlasmaDeflector,
            UnitDefKind::CoreEnergyWeapon => *self == Self::EnergyWeapon,
            UnitDefKind::CoreRadarTargeting => *self == Self::RadarTargeting,
            UnitDefKind::CorePopUpBattery => *self == Self::PopUpBattery,
            UnitDefKind::CorePopUpPlasmaArtillery => *self == Self::PopUpPlasmaArtillery,
            UnitDefKind::CoreAntiAirFlak => *self == Self::AntiAirFlak,
            UnitDefKind::CoreLongRangeAntiAir => *self == Self::LongRangeAntiAir,
            UnitDefKind::CoreSeaplanePlatform => *self == Self::SeaplanePlatform,
            UnitDefKind::CoreTacticalMissileLauncher => *self == Self::TacticalMissileLauncher,
            UnitDefKind::CoreNuclearSilo => *self == Self::NuclearSilo,
            UnitDefKind::CoreAntiNukeLauncher => *self == Self::AntiNukeLauncher,
            UnitDefKind::CoreLongRangePlasmaCannon => *self == Self::LongRangePlasmaCannon,
            UnitDefKind::CoreRapidFireLongRangePlasmaCannon => {
                *self == Self::RapidFireLongRangePlasmaCannon
            }
            UnitDefKind::CoreExperimentalGantry => *self == Self::ExperimentalGantry,
            UnitDefKind::CoreT1BotConstructor => *self == Self::T1BotConstructor,
            UnitDefKind::CoreT1RessurectionBot => *self == Self::T1RessurectionBot,
            UnitDefKind::CoreFastInfantryBot => *self == Self::FastInfantryBot,
            UnitDefKind::CoreRocketBot => *self == Self::RocketBot,
            UnitDefKind::CoreLightPlasmaBot => *self == Self::LightPlasmaBot,
            UnitDefKind::CoreAmphibiousAntiAirBot => *self == Self::AmphibiousAntiAirBot,
            UnitDefKind::CoreAdvancedBotLab => *self == Self::AdvancedBotLab,
            UnitDefKind::CoreT2BotConstructor => *self == Self::T2BotConstructor,
            UnitDefKind::CoreFastAssaultBot => *self == Self::FastAssaultBot,
            UnitDefKind::CoreAmphibiousBot => *self == Self::AmphibiousBot,
            UnitDefKind::CoreHeavilyArmoredAssaultBot => *self == Self::HeavilyArmoredAssaultBot,
            UnitDefKind::CoreArmoredAssaultBot => *self == Self::ArmoredAssaultBot,
            UnitDefKind::CoreSpiderBot => *self == Self::SpiderBot,
            UnitDefKind::CoreMortarBot => *self == Self::MortarBot,
            UnitDefKind::CoreCrawlingBomb => *self == Self::CrawlingBomb,
            UnitDefKind::CoreHeavyRocketBot => *self == Self::HeavyRocketBot,
            UnitDefKind::CoreAdvancedCrawlingBomb => *self == Self::AdvancedCrawlingBomb,
            UnitDefKind::CoreDecoyCommander => *self == Self::DecoyCommander,
            UnitDefKind::CoreRadarBot => *self == Self::RadarBot,
            UnitDefKind::CoreSpyBot => *self == Self::SpyBot,
            UnitDefKind::CoreStealthBot => *self == Self::StealthBot,
            UnitDefKind::CoreCombatEngineer => *self == Self::CombatEngineer,
            UnitDefKind::CoreJammerBot => *self == Self::JammerBot,
            UnitDefKind::CoreHeavyAntiAir => *self == Self::HeavyAntiAir,
            UnitDefKind::CoreT1VehicleConstructor => *self == Self::T1VehicleConstructor,
            UnitDefKind::CoreAmphibiousConstructor => *self == Self::AmphibiousConstructor,
            UnitDefKind::CoreMinelayer => *self == Self::Minelayer,
            UnitDefKind::CoreLightScoutVehicle => *self == Self::LightScoutVehicle,
            UnitDefKind::CoreLightTank => *self == Self::LightTank,
            UnitDefKind::CoreLightAmphibiousTank => *self == Self::LightAmphibiousTank,
            UnitDefKind::CoreMediumAssaultTank => *self == Self::MediumAssaultTank,
            UnitDefKind::CoreLightMobileArtillery => *self == Self::LightMobileArtillery,
            UnitDefKind::CoreMissileTruck => *self == Self::MissileTruck,
            UnitDefKind::CoreAntiSwarmTank => *self == Self::AntiSwarmTank,
            UnitDefKind::CoreLightMine => *self == Self::LightMine,
            UnitDefKind::CoreMediumMine => *self == Self::MediumMine,
            UnitDefKind::CoreHeavyMine => *self == Self::HeavyMine,
            UnitDefKind::CoreAdvancedVehiclePlant => *self == Self::AdvancedVehiclePlant,
            UnitDefKind::CoreT2VehicleConstructor => *self == Self::T2VehicleConstructor,
            UnitDefKind::CoreRadarJammerVehicle => *self == Self::RadarJammerVehicle,
            UnitDefKind::CoreHeavyAssaultTank => *self == Self::HeavyAssaultTank,
            UnitDefKind::CoreStealthyRocketLauncher => *self == Self::StealthyRocketLauncher,
            UnitDefKind::CoreHeavyMissileTank => *self == Self::HeavyMissileTank,
            UnitDefKind::CoreMobileArtilleryTank => *self == Self::MobileArtilleryTank,
            UnitDefKind::CoreHeavyArtilleryTank => *self == Self::HeavyArtilleryTank,
            UnitDefKind::CoreVeryHeavyAssaultTank => *self == Self::VeryHeavyAssaultTank,
            UnitDefKind::CoreMediumAmphibiousTank => *self == Self::MediumAmphibiousTank,
            UnitDefKind::CoreVeryHeavyAmphibiousTank => *self == Self::VeryHeavyAmphibiousTank,
            UnitDefKind::CoreAntiAirFlakTank => *self == Self::AntiAirFlakTank,
            UnitDefKind::CoreAntiNukeTank => *self == Self::AntiNukeTank,
            UnitDefKind::CoreRadarVehicle => *self == Self::RadarVehicle,
            UnitDefKind::CoreMobileHeavyTurretExperimental => {
                *self == Self::MobileHeavyTurretExperimental
            }
            UnitDefKind::CoreAllTerrainAssaultExperimental => {
                *self == Self::AllTerrainAssaultExperimental
            }
            UnitDefKind::CoreAssaultBotExperimental => *self == Self::AssaultBotExperimental,
            UnitDefKind::CoreAmphibiousSiegeExperimental => {
                *self == Self::AmphibiousSiegeExperimental
            }
            UnitDefKind::CoreHeavyRocketExperimental => *self == Self::HeavyRocketExperimental,
            UnitDefKind::CoreHeavyLaserHovertankExperimental => {
                *self == Self::HeavyLaserHovertankExperimental
            }
            UnitDefKind::CoreT1HovercraftConstructor => *self == Self::T1HovercraftConstructor,
            UnitDefKind::CoreFastAttackHovertank => *self == Self::FastAttackHovertank,
            UnitDefKind::CoreHovertank => *self == Self::Hovertank,
            UnitDefKind::CoreAssaultHovertank => *self == Self::AssaultHovertank,
            UnitDefKind::CoreRocketHovertank => *self == Self::RocketHovertank,
            UnitDefKind::CoreAntiAirHovertank => *self == Self::AntiAirHovertank,
            UnitDefKind::CoreNavalConstructionTurret => *self == Self::NavalConstructionTurret,
            UnitDefKind::CoreAmphibiousComplex => *self == Self::AmphibiousComplex,
            UnitDefKind::CoreFloatingHeavyLaserTower => *self == Self::FloatingHeavyLaserTower,
            UnitDefKind::CoreAdvancedShipyard => *self == Self::AdvancedShipyard,
            UnitDefKind::CoreUnderwaterGeothermalPowerplant => {
                *self == Self::UnderwaterGeothermalPowerplant
            }
            UnitDefKind::CorePopUpTorpedoLauncher => *self == Self::PopUpTorpedoLauncher,
            UnitDefKind::CoreT1ShipConstructor => *self == Self::T1ShipConstructor,
            UnitDefKind::CoreRessurectionSub => *self == Self::RessurectionSub,
            UnitDefKind::CoreLightGunBoat => *self == Self::LightGunBoat,
            UnitDefKind::CoreMissileCorvette => *self == Self::MissileCorvette,
            UnitDefKind::CoreAssaultFrigate => *self == Self::AssaultFrigate,
            UnitDefKind::CoreDestroyer => *self == Self::Destroyer,
            UnitDefKind::CoreSub => *self == Self::Sub,
            UnitDefKind::CoreT2ShipConstructor => *self == Self::T2ShipConstructor,
            UnitDefKind::CoreNavalEngineer => *self == Self::NavalEngineer,
            UnitDefKind::CoreCruiser => *self == Self::Cruiser,
            UnitDefKind::CoreFastAssaultSubmarine => *self == Self::FastAssaultSubmarine,
            UnitDefKind::CoreLongRangeBattleSubmarine => *self == Self::LongRangeBattleSubmarine,
            UnitDefKind::CoreAntiAirShip => *self == Self::AntiAirShip,
            UnitDefKind::CoreRadarJammerShip => *self == Self::RadarJammerShip,
            UnitDefKind::CoreAircraftCarrier => *self == Self::AircraftCarrier,
            UnitDefKind::CoreBattleship => *self == Self::Battleship,
            UnitDefKind::CoreCruiseMissileShip => *self == Self::CruiseMissileShip,
            UnitDefKind::CoreFlagship => *self == Self::Flagship,
            UnitDefKind::CoreFloatingHeavyMine => *self == Self::FloatingHeavyMine,
            UnitDefKind::CoreUnderwaterExperimentalGantry => {
                *self == Self::UnderwaterExperimentalGantry
            }
            UnitDefKind::CoreUnderwaterFusionReactor => *self == Self::UnderwaterFusionReactor,
            UnitDefKind::CoreUnderwaterMetalConverter => *self == Self::UnderwaterMetalConverter,
            UnitDefKind::CoreUnderwaterEnergyConverter => *self == Self::UnderwaterEnergyConverter,
            UnitDefKind::CoreAdvancedSonar => *self == Self::AdvancedSonar,
            UnitDefKind::CoreNavalAdvancedRadarTargeting => {
                *self == Self::NavalAdvancedRadarTargeting
            }
            UnitDefKind::CoreAdvancedTorpedoLauncher => *self == Self::AdvancedTorpedoLauncher,
            UnitDefKind::CoreNavalAntiAirGun => *self == Self::NavalAntiAirGun,
            UnitDefKind::CoreFloatingMultiweaponPlatform => {
                *self == Self::FloatingMultiweaponPlatform
            }
            UnitDefKind::CoreSeaplaneConstructor => *self == Self::SeaplaneConstructor,
            UnitDefKind::CoreSeaplaneGunship => *self == Self::SeaplaneGunship,
            UnitDefKind::CoreSeaplaneBomber => *self == Self::SeaplaneBomber,
            UnitDefKind::CoreSeaplaneTorpedoGunship => *self == Self::SeaplaneTorpedoGunship,
            UnitDefKind::CoreSeaplaneFighter => *self == Self::SeaplaneFighter,
            UnitDefKind::CoreSeaplaneRadarSonar => *self == Self::SeaplaneRadarSonar,
            UnitDefKind::Unknown => *self == Self::Unknown,
            _ => false,
        }
    }
}
