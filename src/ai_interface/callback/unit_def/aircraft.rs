use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitAircraft {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum HoverFactor {
    CanLand(u32),
    HoverDrift(u32),
}

impl From<i32> for HoverFactor {
    fn from(i: i32) -> Self {
        if i < 0 {
            HoverFactor::CanLand((i * -1) as u32)
        } else {
            HoverFactor::HoverDrift(i as u32)
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitAircraftAll {
    pub wing_drag: f32,
    pub wing_angle: f32,
    pub front_to_speed: f32,
    pub speed_to_front: f32,
    pub gravity: f32,
    pub max_bank: f32,
    pub max_pitch: f32,
    pub turn_radius: f32,
    pub wanted_height: f32,
    pub vertical_speed: f32,
    pub max_acceleration: f32,
    pub max_deceleration: f32,
    pub max_aileron: f32,
    pub max_elevator: f32,
    pub max_rudder: f32,
    pub dl_hover_factor: f32,
}

impl UnitAircraft {
    pub fn wing_drag(&self) -> Result<f32, Box<dyn Error>> {
        let get_wing_drag_func = get_callback!(self.ai_id, UnitDef_getWingDrag)?;
        Ok(unsafe { get_wing_drag_func(self.ai_id, self.def_id) })
    }

    pub fn wing_angle(&self) -> Result<f32, Box<dyn Error>> {
        let get_wing_angle_func = get_callback!(self.ai_id, UnitDef_getWingAngle)?;
        Ok(unsafe { get_wing_angle_func(self.ai_id, self.def_id) })
    }

    pub fn front_to_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_front_to_speed_func = get_callback!(self.ai_id, UnitDef_getFrontToSpeed)?;
        Ok(unsafe { get_front_to_speed_func(self.ai_id, self.def_id) })
    }

    pub fn speed_to_front(&self) -> Result<f32, Box<dyn Error>> {
        let get_speed_to_front_func = get_callback!(self.ai_id, UnitDef_getSpeedToFront)?;
        Ok(unsafe { get_speed_to_front_func(self.ai_id, self.def_id) })
    }

    pub fn gravity(&self) -> Result<f32, Box<dyn Error>> {
        let get_my_gravity_func = get_callback!(self.ai_id, UnitDef_getMyGravity)?;
        Ok(unsafe { get_my_gravity_func(self.ai_id, self.def_id) })
    }

    pub fn max_bank(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_bank_func = get_callback!(self.ai_id, UnitDef_getMaxBank)?;
        Ok(unsafe { get_max_bank_func(self.ai_id, self.def_id) })
    }

    pub fn max_pitch(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_pitch_func = get_callback!(self.ai_id, UnitDef_getMaxPitch)?;
        Ok(unsafe { get_max_pitch_func(self.ai_id, self.def_id) })
    }

    pub fn turn_radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_turn_radius_func = get_callback!(self.ai_id, UnitDef_getTurnRadius)?;
        Ok(unsafe { get_turn_radius_func(self.ai_id, self.def_id) })
    }

    pub fn wanted_height(&self) -> Result<f32, Box<dyn Error>> {
        let get_wanted_height_func = get_callback!(self.ai_id, UnitDef_getWantedHeight)?;
        Ok(unsafe { get_wanted_height_func(self.ai_id, self.def_id) })
    }

    pub fn vertical_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_vertical_speed_func = get_callback!(self.ai_id, UnitDef_getVerticalSpeed)?;
        Ok(unsafe { get_vertical_speed_func(self.ai_id, self.def_id) })
    }

    pub fn max_acceleration(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_acceleration_func = get_callback!(self.ai_id, UnitDef_getMaxAcceleration)?;
        Ok(unsafe { get_max_acceleration_func(self.ai_id, self.def_id) })
    }

    pub fn max_deceleration(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_deceleration_func = get_callback!(self.ai_id, UnitDef_getMaxDeceleration)?;
        Ok(unsafe { get_max_deceleration_func(self.ai_id, self.def_id) })
    }

    pub fn max_aileron(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_aileron_func = get_callback!(self.ai_id, UnitDef_getMaxAileron)?;
        Ok(unsafe { get_max_aileron_func(self.ai_id, self.def_id) })
    }

    pub fn max_elevator(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_elevator_func = get_callback!(self.ai_id, UnitDef_getMaxElevator)?;
        Ok(unsafe { get_max_elevator_func(self.ai_id, self.def_id) })
    }

    pub fn max_rudder(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_rudderfunc = get_callback!(self.ai_id, UnitDef_getMaxRudder)?;
        Ok(unsafe { get_max_rudderfunc(self.ai_id, self.def_id) })
    }

    pub fn dl_hover_factor(&self) -> Result<f32, Box<dyn Error>> {
        let get_dl_hover_factor_func = get_callback!(self.ai_id, UnitDef_getDlHoverFactor)?;
        Ok(unsafe { get_dl_hover_factor_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitAircraftAll, Box<dyn Error>> {
        Ok(UnitAircraftAll {
            wing_drag: self.wing_drag()?,
            wing_angle: self.wing_angle()?,
            front_to_speed: self.front_to_speed()?,
            speed_to_front: self.speed_to_front()?,
            gravity: self.gravity()?,
            max_bank: self.max_bank()?,
            max_pitch: self.max_pitch()?,
            turn_radius: self.turn_radius()?,
            wanted_height: self.wanted_height()?,
            vertical_speed: self.vertical_speed()?,
            max_acceleration: self.max_acceleration()?,
            max_deceleration: self.max_deceleration()?,
            max_aileron: self.max_aileron()?,
            max_elevator: self.max_elevator()?,
            max_rudder: self.max_rudder()?,
            dl_hover_factor: self.dl_hover_factor()?,
        })
    }
}
