use std::{error::Error, ffi::CStr};

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitDeath {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitDeathAll {
    pub wreck_name: String,
    pub death_explosion: i32,
    pub self_d_explosion: i32,
    pub able_to_self_d: bool,
    pub self_d_countdown: i32,
}

impl UnitDeath {
    pub fn wreck_name(&self) -> Result<String, Box<dyn Error>> {
        let get_wreck_name_func = get_callback!(self.ai_id, UnitDef_getWreckName)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_wreck_name_func(self.ai_id, self.def_id)) }.to_str()?,
        ))
    }

    pub fn death_explosion(&self) -> Result<i32, Box<dyn Error>> {
        let get_death_explosion_func = get_callback!(self.ai_id, UnitDef_getDeathExplosion)?;
        Ok(unsafe { get_death_explosion_func(self.ai_id, self.def_id) })
    }

    pub fn self_d_explosion(&self) -> Result<i32, Box<dyn Error>> {
        let get_self_d_explosion_func = get_callback!(self.ai_id, UnitDef_getSelfDExplosion)?;
        Ok(unsafe { get_self_d_explosion_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_self_d(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isAbleToSelfD)?;
        Ok(unsafe { get_is_able_to_self_d_func(self.ai_id, self.def_id) })
    }

    pub fn self_d_countdown(&self) -> Result<i32, Box<dyn Error>> {
        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_getSelfDCountdown)?;
        Ok(unsafe { get_is_able_to_self_d_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitDeathAll, Box<dyn Error>> {
        Ok(UnitDeathAll {
            wreck_name: self.wreck_name()?,
            death_explosion: self.death_explosion()?,
            self_d_explosion: self.self_d_explosion()?,
            able_to_self_d: self.able_to_self_d()?,
            self_d_countdown: self.self_d_countdown()?,
        })
    }
}
