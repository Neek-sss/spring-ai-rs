use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::{ai_interface::callback::resource::Resource, get_callback};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitResource {
    pub ai_id: i32,
    pub def_id: i32,
    pub resource: Resource,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitResourceAll {
    pub generated: f32,
    pub active_generated: f32,
    pub extracts: f32,
    pub extractor_range: f32,
    pub wind_resource_generator: f32,
    pub tidal_resource_generator: f32,
    pub storage: f32,
}

impl UnitResource {
    pub fn generated(&self) -> Result<f32, Box<dyn Error>> {
        let get_resource_make_func = get_callback!(self.ai_id, UnitDef_getResourceMake)?;
        Ok(unsafe { get_resource_make_func(self.ai_id, self.def_id, self.resource.resource_id) })
    }

    pub fn active_generated(&self) -> Result<f32, Box<dyn Error>> {
        let get_makes_resource_func = get_callback!(self.ai_id, UnitDef_getMakesResource)?;
        Ok(unsafe { get_makes_resource_func(self.ai_id, self.def_id, self.resource.resource_id) })
    }

    pub fn extracts(&self) -> Result<f32, Box<dyn Error>> {
        let get_extracts_func = get_callback!(self.ai_id, UnitDef_getExtractsResource)?;
        Ok(unsafe { get_extracts_func(self.ai_id, self.def_id, self.resource.resource_id) })
    }

    pub fn extractor_range(&self) -> Result<f32, Box<dyn Error>> {
        let get_resource_extractor_range_func =
            get_callback!(self.ai_id, UnitDef_getResourceExtractorRange)?;
        Ok(unsafe {
            get_resource_extractor_range_func(self.ai_id, self.def_id, self.resource.resource_id)
        })
    }

    pub fn wind_resource_generator(&self) -> Result<f32, Box<dyn Error>> {
        let get_wind_resource_generator_func =
            get_callback!(self.ai_id, UnitDef_getWindResourceGenerator)?;
        Ok(unsafe {
            get_wind_resource_generator_func(self.ai_id, self.def_id, self.resource.resource_id)
        })
    }

    pub fn tidal_resource_generator(&self) -> Result<f32, Box<dyn Error>> {
        let get_tidal_resource_generator_func =
            get_callback!(self.ai_id, UnitDef_getTidalResourceGenerator)?;
        Ok(unsafe {
            get_tidal_resource_generator_func(self.ai_id, self.def_id, self.resource.resource_id)
        })
    }

    pub fn storage(&self) -> Result<f32, Box<dyn Error>> {
        let get_storage_func = get_callback!(self.ai_id, UnitDef_getStorage)?;
        Ok(unsafe { get_storage_func(self.ai_id, self.def_id, self.resource.resource_id) })
    }

    pub fn all(&self) -> Result<UnitResourceAll, Box<dyn Error>> {
        Ok(UnitResourceAll {
            generated: self.generated()?,
            active_generated: self.active_generated()?,
            extracts: self.extracts()?,
            extractor_range: self.extractor_range()?,
            wind_resource_generator: self.wind_resource_generator()?,
            tidal_resource_generator: self.tidal_resource_generator()?,
            storage: self.storage()?,
        })
    }
}
