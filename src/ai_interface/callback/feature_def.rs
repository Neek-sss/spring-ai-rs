use std::{collections::HashMap, error::Error, ffi::CStr};

use serde::{Deserialize, Serialize};

use crate::{
    ai_interface::{
        callback::resource::{Resource, ResourceInterface},
        AIInterface,
    },
    get_callback,
};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct FeatureDef {
    pub ai_id: i32,
    pub feature_def_id: i32,
}

#[derive(Debug, Clone)]
pub struct FeatureDefInterface {
    pub ai_id: i32,
}

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum FeatureDefResurrectable {
    Yes,
    No,
    Err,
}

#[derive(Debug, Clone)]
pub struct FeatureDefAll {
    name: String,
    description: String,
    contained_resource: HashMap<Resource, f32>,
    reclaim_time: f32,
    mass: f32,
    upright: bool,
    draw_type: i32,
    resurrectable: FeatureDefResurrectable,
    smoke_time: i32,
    destructable: bool,
    reclaimable: bool,
    blocking: bool,
    burnable: bool,
    floating: bool,
    no_select: bool,
    geo_thermal: bool,
    x_size: i32,
    z_size: i32,
}

impl AIInterface {
    pub fn feature_def_interface(&self) -> FeatureDefInterface {
        FeatureDefInterface { ai_id: self.ai_id }
    }
}

const MAX_FEATURE_DEFS: usize = 128;

impl FeatureDefInterface {
    pub fn get_feature_defs(&self) -> Result<Vec<FeatureDef>, Box<dyn Error>> {
        let get_feature_defs = get_callback!(self.ai_id, getFeatureDefs)?;

        let mut ret = [-1_i32; MAX_FEATURE_DEFS];
        unsafe { get_feature_defs(self.ai_id, ret.as_mut_ptr(), MAX_FEATURE_DEFS as i32) };

        Ok(ret
            .iter()
            .filter_map(|&i| {
                if i == -1 {
                    None
                } else {
                    Some(FeatureDef {
                        ai_id: self.ai_id,
                        feature_def_id: i,
                    })
                }
            })
            .collect())
    }
}

impl FeatureDef {
    pub fn name(&self) -> Result<String, Box<dyn Error>> {
        let get_name = get_callback!(self.ai_id, FeatureDef_getName)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_name(self.ai_id, self.feature_def_id)) }.to_str()?,
        ))
    }

    pub fn description(&self) -> Result<String, Box<dyn Error>> {
        let get_description = get_callback!(self.ai_id, FeatureDef_getDescription)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_description(self.ai_id, self.feature_def_id)) }.to_str()?,
        ))
    }

    pub fn contained_resource(&self) -> Result<HashMap<Resource, f32>, Box<dyn Error>> {
        let get_contained_resource = get_callback!(self.ai_id, FeatureDef_getContainedResource)?;
        Ok(ResourceInterface { ai_id: self.ai_id }
            .get_resources()?
            .into_iter()
            .map(|resource| {
                (resource, unsafe {
                    get_contained_resource(self.ai_id, self.feature_def_id, resource.resource_id)
                })
            })
            .collect())
    }

    pub fn max_health(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_health = get_callback!(self.ai_id, FeatureDef_getMaxHealth)?;
        Ok(unsafe { get_max_health(self.ai_id, self.feature_def_id) })
    }

    pub fn reclaim_time(&self) -> Result<f32, Box<dyn Error>> {
        let get_reclaim_time = get_callback!(self.ai_id, FeatureDef_getReclaimTime)?;
        Ok(unsafe { get_reclaim_time(self.ai_id, self.feature_def_id) })
    }

    pub fn mass(&self) -> Result<f32, Box<dyn Error>> {
        let get_mass = get_callback!(self.ai_id, FeatureDef_getMass)?;
        Ok(unsafe { get_mass(self.ai_id, self.feature_def_id) })
    }

    pub fn upright(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_upright = get_callback!(self.ai_id, FeatureDef_isUpright)?;
        Ok(unsafe { get_is_upright(self.ai_id, self.feature_def_id) })
    }

    pub fn draw_type(&self) -> Result<i32, Box<dyn Error>> {
        let get_draw_type = get_callback!(self.ai_id, FeatureDef_getDrawType)?;
        Ok(unsafe { get_draw_type(self.ai_id, self.feature_def_id) })
    }

    pub fn resurrectable(&self) -> Result<FeatureDefResurrectable, Box<dyn Error>> {
        let get_resurrectable = get_callback!(self.ai_id, FeatureDef_getResurrectable)?;
        Ok(
            match unsafe { get_resurrectable(self.ai_id, self.feature_def_id) } {
                0 => FeatureDefResurrectable::No,
                1 => FeatureDefResurrectable::Yes,
                _ => FeatureDefResurrectable::Err,
            },
        )
    }

    pub fn smoke_time(&self) -> Result<i32, Box<dyn Error>> {
        let get_smoke_time = get_callback!(self.ai_id, FeatureDef_getSmokeTime)?;
        Ok(unsafe { get_smoke_time(self.ai_id, self.feature_def_id) })
    }

    pub fn destructable(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_destructable = get_callback!(self.ai_id, FeatureDef_isDestructable)?;
        Ok(unsafe { get_is_destructable(self.ai_id, self.feature_def_id) })
    }

    pub fn reclaimable(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_reclaimable = get_callback!(self.ai_id, FeatureDef_isReclaimable)?;
        Ok(unsafe { get_is_reclaimable(self.ai_id, self.feature_def_id) })
    }

    pub fn blocking(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_blocking = get_callback!(self.ai_id, FeatureDef_isBlocking)?;
        Ok(unsafe { get_is_blocking(self.ai_id, self.feature_def_id) })
    }

    pub fn burnable(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_burnable = get_callback!(self.ai_id, FeatureDef_isBurnable)?;
        Ok(unsafe { get_is_burnable(self.ai_id, self.feature_def_id) })
    }

    pub fn floating(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_floating = get_callback!(self.ai_id, FeatureDef_isFloating)?;
        Ok(unsafe { get_is_floating(self.ai_id, self.feature_def_id) })
    }

    pub fn no_select(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_no_select = get_callback!(self.ai_id, FeatureDef_isNoSelect)?;
        Ok(unsafe { get_is_no_select(self.ai_id, self.feature_def_id) })
    }

    pub fn geo_thermal(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_geo_thermal = get_callback!(self.ai_id, FeatureDef_isGeoThermal)?;
        Ok(unsafe { get_is_geo_thermal(self.ai_id, self.feature_def_id) })
    }

    pub fn x_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_x_size = get_callback!(self.ai_id, FeatureDef_getXSize)?;
        Ok(unsafe { get_x_size(self.ai_id, self.feature_def_id) })
    }

    pub fn z_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_z_size = get_callback!(self.ai_id, FeatureDef_getZSize)?;
        Ok(unsafe { get_z_size(self.ai_id, self.feature_def_id) })
    }

    pub fn all(&self) -> Result<FeatureDefAll, Box<dyn Error>> {
        Ok(FeatureDefAll {
            name: self.name()?,
            description: self.description()?,
            contained_resource: self.contained_resource()?,
            reclaim_time: self.reclaim_time()?,
            mass: self.mass()?,
            upright: self.upright()?,
            draw_type: self.draw_type()?,
            resurrectable: self.resurrectable()?,
            smoke_time: self.smoke_time()?,
            destructable: self.destructable()?,
            reclaimable: self.reclaimable()?,
            blocking: self.blocking()?,
            burnable: self.burnable()?,
            floating: self.floating()?,
            no_select: self.no_select()?,
            geo_thermal: self.geo_thermal()?,
            x_size: self.x_size()?,
            z_size: self.z_size()?,
        })
    }
}
