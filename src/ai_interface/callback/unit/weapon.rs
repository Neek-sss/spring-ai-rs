use std::error::Error;

use crate::{ai_interface::callback::weapon_def::WeaponDef, get_callback};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct UnitWeapon {
    pub ai_id: i32,
    pub unit_id: i32,
    pub weapon_id: i32,
}

#[derive(Debug, Clone)]
pub struct UnitWeaponInterface {
    pub ai_id: i32,
}

#[derive(Debug, Clone)]
pub struct UnitWeaponAll {
    weapon_def_id: WeaponDef,
    reload_frame: i32,
    reload_time: i32,
    range: f32,
    shield_enabled: bool,
    shield_power: f32,
}

const MAX_WEAPON_DEFS: usize = 128;

impl UnitWeapon {
    pub fn weapon_def(&self) -> Result<WeaponDef, Box<dyn Error>> {
        let get_weapon_def = get_callback!(self.ai_id, Unit_Weapon_getReloadFrame)?;
        let weapon_def_id = unsafe { get_weapon_def(self.ai_id, self.unit_id, self.weapon_id) };
        Ok(WeaponDef {
            ai_id: self.ai_id,
            weapon_def_id,
        })
    }

    pub fn reload_frame(&self) -> Result<i32, Box<dyn Error>> {
        let get_reload_frame = get_callback!(self.ai_id, Unit_Weapon_getReloadFrame)?;
        Ok(unsafe { get_reload_frame(self.ai_id, self.unit_id, self.weapon_id) })
    }

    pub fn reload_time(&self) -> Result<i32, Box<dyn Error>> {
        let get_reload_time = get_callback!(self.ai_id, Unit_Weapon_getReloadTime)?;
        Ok(unsafe { get_reload_time(self.ai_id, self.unit_id, self.weapon_id) })
    }

    pub fn range(&self) -> Result<f32, Box<dyn Error>> {
        let get_reload_range = get_callback!(self.ai_id, Unit_Weapon_getRange)?;
        Ok(unsafe { get_reload_range(self.ai_id, self.unit_id, self.weapon_id) })
    }

    pub fn shield_enabled(&self) -> Result<bool, Box<dyn Error>> {
        let get_shield_enabled = get_callback!(self.ai_id, Unit_Weapon_isShieldEnabled)?;
        Ok(unsafe { get_shield_enabled(self.ai_id, self.unit_id, self.weapon_id) })
    }

    pub fn shield_power(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_power = get_callback!(self.ai_id, Unit_Weapon_getShieldPower)?;
        Ok(unsafe { get_shield_power(self.ai_id, self.unit_id, self.weapon_id) })
    }

    pub fn all(&self) -> Result<UnitWeaponAll, Box<dyn Error>> {
        Ok(UnitWeaponAll {
            weapon_def_id: self.weapon_def()?,
            reload_frame: self.reload_frame()?,
            reload_time: self.reload_time()?,
            range: self.range()?,
            shield_enabled: self.shield_enabled()?,
            shield_power: self.shield_power()?,
        })
    }
}
