use std::ffi::CString;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            drawer::{AddPointDrawCommandData, RemovePointDrawCommandData},
            CommandData,
        },
        command_topic::CommandTopic,
    },
    engine::handle_command,
};

pub struct Point {
    pub ai_id: i32,
}

impl Point {
    pub fn add_point(&self, position: [f32; 3], label: &str) -> Result<(), String> {
        let mut command_c_data = AddPointDrawCommandData {
            position,
            label: CString::new(label).map_err(|e| e.to_string())?,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPointAdd.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn remove_point(&self, position: [f32; 3]) -> Result<(), String> {
        let mut command_c_data = RemovePointDrawCommandData { position }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPointRemove.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
