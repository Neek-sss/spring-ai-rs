use std::ffi::CString;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::{
    callback::{
        command::{
            command_data::{
                debug_drawer::{
                    AddOverlayTextureDrawerDebugCommandData,
                    DeleteOverlayTextureDrawerDebugCommandData,
                    SetLabelOverlayTextureDrawerDebugCommandData,
                    SetPositionOverlayTextureDrawerDebugCommandData,
                    SetSizeOverlayTextureDrawerDebugCommandData,
                    UpdateOverlayTextureDrawerDebugCommandData,
                },
                CommandData,
            },
            command_topic::CommandTopic,
        },
        engine::handle_command,
    },
    AIInterface,
};

impl AIInterface {
    pub fn add_overlay_texture(
        &self,
        texture_data: Vec<f32>,
        w: i32,
        h: i32,
    ) -> Result<OverlayTexture, String> {
        OverlayTexture::add(self.ai_id, texture_data, w, h)
    }
}

pub struct OverlayTexture {
    pub ai_id: i32,
    pub texture_id: i32,
}

impl OverlayTexture {
    fn add(ai_id: i32, texture_data: Vec<f32>, w: i32, h: i32) -> Result<Self, String> {
        let mut command_c_data =
            AddOverlayTextureDrawerDebugCommandData { texture_data, w, h }.c_data();

        handle_command(
            ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureAdd.into(),
            &mut command_c_data,
        )?;

        Ok(Self {
            ai_id,
            texture_id: command_c_data.ret_overlayTextureId,
        })
    }
    pub fn delete(&self) -> Result<(), String> {
        let mut command_c_data = DeleteOverlayTextureDrawerDebugCommandData {
            overlay_texture_id: self.texture_id,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureDelete.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_label(&self, label: &str) -> Result<(), String> {
        let mut command_c_data = SetLabelOverlayTextureDrawerDebugCommandData {
            overlay_texture_id: self.texture_id,
            label: CString::new(label).map_err(|e| e.to_string())?,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureSetLabel.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_position(&self, x: f32, y: f32) -> Result<(), String> {
        let mut command_c_data = SetPositionOverlayTextureDrawerDebugCommandData {
            overlay_texture_id: self.texture_id,
            x,
            y,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureSetPOS.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_size(&self, w: f32, h: f32) -> Result<(), String> {
        let mut command_c_data = SetSizeOverlayTextureDrawerDebugCommandData {
            overlay_texture_id: self.texture_id,
            w,
            h,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureSetSize.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn update(
        &self,
        texture_data: Vec<f32>,
        x: i32,
        y: i32,
        w: i32,
        h: i32,
    ) -> Result<(), String> {
        let mut command_c_data = UpdateOverlayTextureDrawerDebugCommandData {
            overlay_texture_id: self.texture_id,
            tex_data: texture_data,
            x,
            y,
            w,
            h,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerOverlaytextureUpdate.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
