use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{drawer::AddLineDrawCommandData, CommandData},
        command_topic::CommandTopic,
    },
    engine::handle_command,
};

pub struct Line {
    pub ai_id: i32,
}

impl Line {
    pub fn add(&self, from: [f32; 3], to: [f32; 3]) -> Result<(), String> {
        let mut command_c_data = AddLineDrawCommandData { to, from }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerLineAdd.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
