use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            drawer::{
                BreakPathDrawerCommandData, DrawIconAtLastPosPathDrawerCommandData,
                DrawLineAndIconPathDrawerCommandData, DrawLinePathDrawerCommandData,
                FinishPathDrawerCommandData, RestartPathDrawerCommandData,
                StartPathDrawerCommandData,
            },
            CommandData,
        },
        command_topic::CommandTopic,
    },
    engine::handle_command,
};

pub struct Path {
    pub ai_id: i32,
}

impl Path {
    pub fn path_break(
        &self,
        end_position: [f32; 3],
        color: [i16; 3],
        alpha: i16,
    ) -> Result<(), String> {
        let mut command_c_data = BreakPathDrawerCommandData {
            end_position,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathBreak.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn draw_icon_at_last_position(&self, command_id: i32) -> Result<(), String> {
        let mut command_c_data = DrawIconAtLastPosPathDrawerCommandData { command_id }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathDrawIconAtLastPOS.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn draw_line(
        &self,
        end_position: [f32; 3],
        color: [i16; 3],
        alpha: i16,
    ) -> Result<(), String> {
        let mut command_c_data = DrawLinePathDrawerCommandData {
            end_position,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathDrawLine.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn draw_line_and_icon(
        &self,
        command_id: i32,
        position: [f32; 3],
        color: [i16; 3],
        alpha: i16,
    ) -> Result<(), String> {
        let mut command_c_data = DrawLineAndIconPathDrawerCommandData {
            command_id,
            position,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathDrawLineAndIcon.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn finish(&self) -> Result<(), String> {
        let mut command_c_data = FinishPathDrawerCommandData {
            i_am_useless: false,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathFinish.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn restart(&self, same_color: bool) -> Result<(), String> {
        let mut command_c_data = RestartPathDrawerCommandData { same_color }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathRestart.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn start(&self, position: [f32; 3], color: [i16; 3], alpha: i16) -> Result<(), String> {
        let mut command_c_data = StartPathDrawerCommandData {
            position,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerPathStart.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
