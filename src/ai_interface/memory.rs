use std::{collections::HashMap, sync::RwLock};

use lazy_static::lazy_static;
use serde::{de::DeserializeOwned, Serialize};

use crate::ai_interface::AIInterface;

lazy_static! {
    static ref AI_STORAGE: RwLock<HashMap<i32, HashMap<String, Vec<u8>>>> =
        RwLock::new(HashMap::new());
}

impl AIInterface {
    pub fn store_memory<T: Serialize + DeserializeOwned>(&self, name: &str, data: &T) {
        let ai_id = self.ai_id;

        {
            let mut storage = AI_STORAGE.try_write().unwrap();
            storage
                .entry(ai_id)
                .or_insert_with(Default::default)
                .insert(name.to_string(), bincode::serialize(data).unwrap());
        }
    }

    pub fn load_memory<T: Serialize + DeserializeOwned>(&self, name: &str) -> Option<T> {
        let ai_id = self.ai_id;

        {
            let mut storage = AI_STORAGE.try_write().unwrap();
            storage
                .entry(ai_id)
                .or_insert_with(Default::default)
                .get(&name.to_string())
                .map(|d| bincode::deserialize(d).unwrap())
        }
    }
}
