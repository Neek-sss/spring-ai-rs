use std::error::Error;

use slog::{trace, Logger};
use spring_ai_sys::{
    SUnitCapturedEvent, SUnitCreatedEvent, SUnitDamagedEvent, SUnitDestroyedEvent,
    SUnitFinishedEvent, SUnitGivenEvent, SUnitIdleEvent, SUnitMoveFailedEvent,
};

use crate::{
    ai_interface::{
        callback::{teams::Team, unit::Unit, weapon_def::WeaponDef},
        AIInterface,
    },
    event::void_to_event,
};

pub type UnitCapturedFuncType =
    fn(&Logger, AIInterface, Unit, Team, Team) -> Result<(), Box<dyn Error>>;

pub fn unit_captured_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_captured_func: &UnitCapturedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_captured_data = void_to_event::<SUnitCapturedEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_captured_data,);

    unit_captured_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_captured_data.unitId,
            ai_id: skirmish_ai_id,
        },
        Team {
            ai_id: skirmish_ai_id,
            team_id: unit_captured_data.oldTeamId,
        },
        Team {
            ai_id: skirmish_ai_id,
            team_id: unit_captured_data.newTeamId,
        },
    )
}

pub type UnitCreatedFuncType = fn(&Logger, AIInterface, Unit, Unit) -> Result<(), Box<dyn Error>>;

pub fn unit_created_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_created_func: &UnitCreatedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_created_data = void_to_event::<SUnitCreatedEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_created_data,);

    unit_created_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_created_data.unit,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: unit_created_data.builder,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type UnitDamagedFuncType = fn(
    &Logger,
    AIInterface,
    Unit,
    Unit,
    f32,
    [f32; 3],
    WeaponDef,
    bool,
) -> Result<(), Box<dyn Error>>;

pub fn unit_damaged_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_damaged_func: &UnitDamagedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_damaged_data = void_to_event::<SUnitDamagedEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_damaged_data,);
    let direction = unsafe { Vec::from_raw_parts(unit_damaged_data.dir_posF3, 3, 3) };

    unit_damaged_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_damaged_data.unit,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: unit_damaged_data.attacker,
            ai_id: skirmish_ai_id,
        },
        unit_damaged_data.damage,
        [direction[0], direction[1], direction[2]],
        WeaponDef {
            weapon_def_id: unit_damaged_data.weaponDefId,
            ai_id: skirmish_ai_id,
        },
        unit_damaged_data.paralyzer,
    )
}

pub type UnitDestroyedFuncType = fn(&Logger, AIInterface, Unit, Unit) -> Result<(), Box<dyn Error>>;

pub fn unit_destroyed_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_destroyed_func: &UnitDestroyedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_destroyed_data = void_to_event::<SUnitDestroyedEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_destroyed_data,);

    unit_destroyed_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_destroyed_data.unit,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: unit_destroyed_data.attacker,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type UnitFinishedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn unit_finished_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_finished_func: &UnitFinishedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_finished_data = void_to_event::<SUnitFinishedEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_finished_data,);

    unit_finished_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_finished_data.unit,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type UnitGivenFuncType =
    fn(&Logger, AIInterface, Unit, Team, Team) -> Result<(), Box<dyn Error>>;

pub fn unit_given_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_given_func: &UnitGivenFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_given_data = void_to_event::<SUnitGivenEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_given_data,);

    unit_given_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_given_data.unitId,
            ai_id: skirmish_ai_id,
        },
        Team {
            ai_id: skirmish_ai_id,
            team_id: unit_given_data.oldTeamId,
        },
        Team {
            ai_id: skirmish_ai_id,
            team_id: unit_given_data.newTeamId,
        },
    )
}

pub type UnitIdleFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn unit_idle_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_idle_func: &UnitIdleFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_idle_data = void_to_event::<SUnitIdleEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, unit_idle_data,);

    unit_idle_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_idle_data.unit,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type UnitMoveFailedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn unit_move_failed_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    unit_move_failed_func: &UnitMoveFailedFuncType,
) -> Result<(), Box<dyn Error>> {
    let unit_move_failed_data = void_to_event::<SUnitMoveFailedEvent>(data as *mut libc::c_void)?;

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        unit_move_failed_data,
    );

    unit_move_failed_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: unit_move_failed_data.unit,
            ai_id: skirmish_ai_id,
        },
    )
}
