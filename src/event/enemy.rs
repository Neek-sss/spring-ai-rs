use std::error::Error;

use slog::{trace, Logger};
use spring_ai_sys::{
    SEnemyCreatedEvent, SEnemyDamagedEvent, SEnemyDestroyedEvent, SEnemyEnterLOSEvent,
    SEnemyEnterRadarEvent, SEnemyFinishedEvent, SEnemyLeaveLOSEvent, SEnemyLeaveRadarEvent,
};

use crate::{
    ai_interface::{
        callback::{unit::Unit, weapon_def::WeaponDef},
        AIInterface,
    },
    event::void_to_event,
};

pub type EnemyCreatedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_created_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_created_func: &EnemyCreatedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_created_data =
        void_to_event::<SEnemyCreatedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_created_data,);

    enemy_created_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_created_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyDamagedFuncType =
    fn(&Logger, AIInterface, Unit, Unit, [f32; 3], WeaponDef, bool) -> Result<(), Box<dyn Error>>;

pub fn enemy_damaged_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_damaged_func: &EnemyDamagedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_damaged_data =
        void_to_event::<SEnemyDamagedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_damaged_data,);
    let direction = unsafe { Vec::from_raw_parts(enemy_damaged_data.dir_posF3, 3, 3) };

    enemy_damaged_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_damaged_data.enemy,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: enemy_damaged_data.attacker,
            ai_id: skirmish_ai_id,
        },
        [direction[0], direction[1], direction[2]],
        WeaponDef {
            weapon_def_id: enemy_damaged_data.weaponDefId,
            ai_id: skirmish_ai_id,
        },
        enemy_damaged_data.paralyzer,
    )
}

pub type EnemyDestroyedFuncType =
    fn(&Logger, AIInterface, Unit, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_destroyed_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_destroyed_func: &EnemyDestroyedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_destroyed_data =
        void_to_event::<SEnemyDestroyedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_destroyed_data,);

    enemy_destroyed_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_destroyed_data.enemy,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: enemy_destroyed_data.attacker,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyEnterLOSFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_enter_los_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_enter_los_func: &EnemyEnterLOSFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_enter_los_data =
        void_to_event::<SEnemyEnterLOSEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_enter_los_data,);

    enemy_enter_los_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_enter_los_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyEnterRadarFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_enter_radar_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_enter_radar_func: &EnemyEnterRadarFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_enter_radar_data =
        void_to_event::<SEnemyEnterRadarEvent>(data as *mut libc::c_void).unwrap();

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        enemy_enter_radar_data,
    );

    enemy_enter_radar_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_enter_radar_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyFinishedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_finished_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_finished_func: &EnemyFinishedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_finished_data =
        void_to_event::<SEnemyFinishedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_finished_data,);

    enemy_finished_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_finished_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyLeaveLOSFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_leave_los_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_leave_los_func: &EnemyLeaveLOSFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_leave_los_data =
        void_to_event::<SEnemyLeaveLOSEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_leave_los_data,);

    enemy_leave_los_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_leave_los_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub type EnemyLeaveRadarFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_leave_radar_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_leave_radar_func: &EnemyLeaveRadarFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_leave_radar_data =
        void_to_event::<SEnemyLeaveRadarEvent>(data as *mut libc::c_void).unwrap();

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        enemy_leave_radar_data,
    );

    enemy_leave_radar_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_leave_radar_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}
