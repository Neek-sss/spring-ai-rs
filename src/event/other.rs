use std::{
    error::Error,
    ffi::{CStr, CString},
};

use slog::{trace, Logger};
use spring_ai_sys::{
    SCommandFinishedEvent, SInitEvent, SLoadEvent, SLuaMessageEvent, SMessageEvent,
    SPlayerCommandEvent, SReleaseEvent, SSaveEvent, SSeismicPingEvent, SUpdateEvent,
    SWeaponFiredEvent,
};

use crate::{
    ai_interface::{
        callback::{
            command::command_topic::CommandTopic,
            group::{init_group_unit_defs, init_group_units},
            unit::Unit,
            weapon_def::WeaponDef,
        },
        AIInterface,
    },
    event::void_to_event,
    skirmish_ai::{remove_skirmish_ai, set_skirmish_ai, SkirmishAI},
};

pub type InitFuncType = fn(&Logger, AIInterface) -> Result<(), Box<dyn Error>>;

pub fn init_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    init_func: InitFuncType,
) -> Result<(), Box<dyn Error>> {
    let init_data = void_to_event::<SInitEvent>(data as *mut libc::c_void)?;

    set_skirmish_ai(skirmish_ai_id, SkirmishAI::new(init_data.callback)?);

    trace!(logger, "AI EVENT INIT");

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, init_data,);

    init_group_unit_defs(skirmish_ai_id)?;
    init_group_units(skirmish_ai_id)?;

    init_func(logger, AIInterface::new(skirmish_ai_id))
}

pub type ReleaseFuncType = fn(&Logger, AIInterface) -> Result<(), Box<dyn Error>>;

pub fn release_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    release_func: &ReleaseFuncType,
) -> Result<(), Box<dyn Error>> {
    let release_data = void_to_event::<SReleaseEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, release_data,);

    let ret = release_func(logger, AIInterface::new(skirmish_ai_id));

    remove_skirmish_ai(skirmish_ai_id);

    ret
}

pub type MessageFuncType = fn(&Logger, AIInterface, i32, &str) -> Result<(), Box<dyn Error>>;

pub fn message_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    message_func: &MessageFuncType,
) -> Result<(), Box<dyn Error>> {
    let message_data = void_to_event::<SMessageEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, message_data,);

    message_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        message_data.player,
        unsafe { CStr::from_ptr(message_data.message) }.to_str()?,
    )
}

pub type CommandFinishedFuncType =
    fn(&Logger, AIInterface, Unit, i32, CommandTopic) -> Result<(), Box<dyn Error>>;

pub fn command_finished_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    command_finished_func: &CommandFinishedFuncType,
) -> Result<(), Box<dyn Error>> {
    let command_finished_data = void_to_event::<SCommandFinishedEvent>(data as *mut libc::c_void)?;

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        command_finished_data,
    );

    command_finished_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: command_finished_data.unitId,
            ai_id: skirmish_ai_id,
        },
        command_finished_data.commandId,
        CommandTopic::from(command_finished_data.commandTopicId),
    )
}

pub type LoadFuncType = fn(&Logger, AIInterface, &str) -> Result<(), Box<dyn Error>>;

pub fn load_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    load_func: &LoadFuncType,
) -> Result<(), Box<dyn Error>> {
    let load_data = void_to_event::<SLoadEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, load_data);

    load_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        &unsafe { CString::from_raw(load_data.file as *mut _) }.to_string_lossy(),
    )
}

pub type LuaMessageFuncType = fn(&Logger, AIInterface, &str) -> Result<(), Box<dyn Error>>;

pub fn lua_message_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    lua_message_func: &LuaMessageFuncType,
) -> Result<(), Box<dyn Error>> {
    let lua_message_data = void_to_event::<SLuaMessageEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, lua_message_data);

    lua_message_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        &unsafe { CString::from_raw(lua_message_data.inData as *mut _) }.to_string_lossy(),
    )
}

pub type NullFuncType = fn(&Logger, AIInterface) -> Result<(), Box<dyn Error>>;

pub fn null_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    null_func: &NullFuncType,
) -> Result<(), Box<dyn Error>> {
    trace!(logger, "ID: {}", skirmish_ai_id,);

    null_func(logger, AIInterface::new(skirmish_ai_id))
}

pub type PlayerCommandFuncType =
    fn(&Logger, AIInterface, &[i32], CommandTopic, i32) -> Result<(), Box<dyn Error>>;

pub fn player_command_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    player_command_func: &PlayerCommandFuncType,
) -> Result<(), Box<dyn Error>> {
    let player_command_data = void_to_event::<SPlayerCommandEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, player_command_data);

    player_command_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        &unsafe {
            Vec::from_raw_parts(
                player_command_data.unitIds,
                player_command_data.unitIds_size as usize,
                player_command_data.unitIds_size as usize,
            )
        },
        CommandTopic::from(player_command_data.commandTopicId),
        player_command_data.playerId,
    )
}

pub type SaveFuncType = fn(&Logger, AIInterface, &str) -> Result<(), Box<dyn Error>>;

pub fn save_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    save_func: &SaveFuncType,
) -> Result<(), Box<dyn Error>> {
    let save_data = void_to_event::<SSaveEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, save_data);

    save_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        &unsafe { CString::from_raw(save_data.file as *mut _) }.to_string_lossy(),
    )
}

pub type SeismicPingFuncType =
    fn(&Logger, AIInterface, [f32; 3], f32) -> Result<(), Box<dyn Error>>;

pub fn seismic_ping_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    seismic_ping_func: &SeismicPingFuncType,
) -> Result<(), Box<dyn Error>> {
    let seismic_ping_data = void_to_event::<SSeismicPingEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, seismic_ping_data,);
    let position = unsafe { Vec::from_raw_parts(seismic_ping_data.pos_posF3, 3, 3) };

    seismic_ping_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        [position[0], position[1], position[2]],
        seismic_ping_data.strength,
    )
}

pub type UpdateFuncType = fn(&Logger, AIInterface, i32) -> Result<(), Box<dyn Error>>;

pub fn update_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    update_func: &UpdateFuncType,
) -> Result<(), Box<dyn Error>> {
    let update_data = void_to_event::<SUpdateEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, update_data,);

    update_func(logger, AIInterface::new(skirmish_ai_id), update_data.frame)
}

pub type WeaponFiredFuncType =
    fn(&Logger, AIInterface, Unit, WeaponDef) -> Result<(), Box<dyn Error>>;

pub fn weapon_fired_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    weapon_fired_func: &WeaponFiredFuncType,
) -> Result<(), Box<dyn Error>> {
    let weapon_fired_data = void_to_event::<SWeaponFiredEvent>(data as *mut libc::c_void)?;

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, weapon_fired_data,);

    weapon_fired_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: weapon_fired_data.unitId,
            ai_id: skirmish_ai_id,
        },
        WeaponDef {
            weapon_def_id: weapon_fired_data.weaponDefId,
            ai_id: skirmish_ai_id,
        },
    )
}
