use linkme::distributed_slice;
pub use spring_ai_rs_macro::ai_func;

#[distributed_slice]
pub static RELEASE: [crate::event::other::ReleaseFuncType] = [..];

#[distributed_slice]
pub static MESSAGE: [crate::event::other::MessageFuncType] = [..];

#[distributed_slice]
pub static COMMAND_FINISHED: [crate::event::other::CommandFinishedFuncType] = [..];

#[distributed_slice]
pub static ENEMY_CREATED: [crate::event::enemy::EnemyCreatedFuncType] = [..];

#[distributed_slice]
pub static ENEMY_DAMAGED: [crate::event::enemy::EnemyDamagedFuncType] = [..];

#[distributed_slice]
pub static ENEMY_DESTROYED: [crate::event::enemy::EnemyDestroyedFuncType] = [..];

#[distributed_slice]
pub static ENEMY_ENTER_LOS: [crate::event::enemy::EnemyEnterLOSFuncType] = [..];

#[distributed_slice]
pub static ENEMY_ENTER_RADAR: [crate::event::enemy::EnemyEnterRadarFuncType] = [..];

#[distributed_slice]
pub static ENEMY_FINISHED: [crate::event::enemy::EnemyFinishedFuncType] = [..];

#[distributed_slice]
pub static ENEMY_LEAVE_LOS: [crate::event::enemy::EnemyLeaveLOSFuncType] = [..];

#[distributed_slice]
pub static ENEMY_LEAVE_RADAR: [crate::event::enemy::EnemyLeaveRadarFuncType] = [..];

#[distributed_slice]
pub static LOAD: [crate::event::other::LoadFuncType] = [..];

#[distributed_slice]
pub static LUA_MESSAGE: [crate::event::other::LuaMessageFuncType] = [..];

#[distributed_slice]
pub static NULL: [crate::event::other::NullFuncType] = [..];

#[distributed_slice]
pub static PLAYER_COMMAND: [crate::event::other::PlayerCommandFuncType] = [..];

#[distributed_slice]
pub static SAVE: [crate::event::other::SaveFuncType] = [..];

#[distributed_slice]
pub static SEISMIC_PING: [crate::event::other::SeismicPingFuncType] = [..];

#[distributed_slice]
pub static UNIT_CAPTURED: [crate::event::unit::UnitCapturedFuncType] = [..];

#[distributed_slice]
pub static UNIT_CREATED: [crate::event::unit::UnitCreatedFuncType] = [..];

#[distributed_slice]
pub static UNIT_DAMAGED: [crate::event::unit::UnitDamagedFuncType] = [..];

#[distributed_slice]
pub static UNIT_DESTROYED: [crate::event::unit::UnitDestroyedFuncType] = [..];

#[distributed_slice]
pub static UNIT_FINISHED: [crate::event::unit::UnitFinishedFuncType] = [..];

#[distributed_slice]
pub static UNIT_GIVEN: [crate::event::unit::UnitGivenFuncType] = [..];

#[distributed_slice]
pub static UNIT_IDLE: [crate::event::unit::UnitIdleFuncType] = [..];

#[distributed_slice]
pub static UNIT_MOVE_FAILED: [crate::event::unit::UnitMoveFailedFuncType] = [..];

#[distributed_slice]
pub static UPDATE: [crate::event::other::UpdateFuncType] = [..];

#[distributed_slice]
pub static WEAPON_FIRED: [crate::event::other::WeaponFiredFuncType] = [..];
